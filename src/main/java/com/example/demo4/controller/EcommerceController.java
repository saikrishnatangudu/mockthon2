package com.example.demo4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.dto.OrderDtoResponse;
import com.example.demo4.model.Order;
import com.example.demo4.service.CommerseService;



@RestController
public class EcommerceController {

	
	@Autowired
	private CommerseService commerseService;
	
	
	@PostMapping("orders")
	public ResponseEntity<OrderDtoResponse>  transac(@RequestBody Order order) { 
		
		OrderDtoResponse orderDtoResponse = commerseService.buyProject(order);
		return new ResponseEntity<>(orderDtoResponse,HttpStatus.OK);
		
	}
	
	
}