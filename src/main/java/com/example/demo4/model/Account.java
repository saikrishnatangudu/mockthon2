package com.example.demo4.model;



import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "account")
public class Account {
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int accountId;
	private long accNo;
	private double amount;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId",nullable = false)
	@JsonIgnore
	private User user;

	public Account() {
		super();
	}

	public Account(int accountId, long accNo, double amount, User user) {
		super();
		this.accountId = accountId;
		this.accNo = accNo;
		this.amount = amount;
		this.user = user;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public long getAccNo() {
		return accNo;
	}

	public void setAccNo(long accNo) {
		this.accNo = accNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

	}
