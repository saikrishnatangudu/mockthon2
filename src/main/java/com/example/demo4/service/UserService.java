package com.example.demo4.service;

import java.util.List;

import com.example.demo4.dto.UserDTO;
import com.example.demo4.dto.UserLoginDto;
import com.example.demo4.exception.UserNotFoundException;
import com.example.demo4.model.User;




public interface UserService {
	
	//public List<User> getAllUsers();
	  public User getUserById(long userId);
	  public User readByUserName(String name);
	  public List<UserDTO> getAllUsers();
	  User login(UserLoginDto userLoginDto) throws UserNotFoundException;


}
