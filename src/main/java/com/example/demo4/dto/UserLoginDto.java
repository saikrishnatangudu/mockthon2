package com.example.demo4.dto;

import com.sun.istack.NotNull;

public class UserLoginDto {
	@NotNull
	private String userName;
	@NotNull
	private String password;

	public UserLoginDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserLoginDto(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserLoginDto [userName=" + userName + ", password=" + password + "]";
	}
}