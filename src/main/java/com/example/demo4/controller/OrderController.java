package com.example.demo4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo4.model.Order;
import com.example.demo4.service.OrderService;



@RestController
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	

	@GetMapping(value = "/orders")
	public ResponseEntity<List<Order>> getAllOrders(Order order) {
		List<Order> orders = orderService.getAllOrders();
		return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
	}
	
	@GetMapping(value = "/order/{id}")
	public ResponseEntity<Order> getOrder(@PathVariable("id") int id) {
		Order order = orderService.getOrderById(id);
		return new ResponseEntity<Order>(order, HttpStatus.OK);

	}

}
