package com.example.demo4.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo4.model.Order;
import com.example.demo4.repository.OrderRepo;


@Service
@Transactional
public class OrderServiceImpl implements OrderService {
	
	
	@Autowired
	private OrderRepo orderRepository;
	

	@Override
	public List<Order> getAllOrders() {
		List<Order> orderList = (List<Order>) orderRepository.findAll();

		if (orderList.size() > 0) {
			return orderList;
		} else {
			return new ArrayList<Order>();
		}
	}

	@Override
	public Order getOrderById(int orderId) {
		
			Optional<Order> option = orderRepository.findById(orderId);
			Order order = null;
			if (option.isPresent()) {
				order = option.get();

			} else {
				return null;
			}
			return order;
		
	}

}
