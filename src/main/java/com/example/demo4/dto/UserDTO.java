package com.example.demo4.dto;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.example.demo4.model.Order;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {
	@JsonProperty(value = "userId")
	private Long UserId;
	@JsonProperty(value = "username")
	private String username;
	@JsonProperty(value = "email")
	private String email;
	@JsonProperty(value = "orders")
	@OneToMany( cascade = CascadeType.ALL, fetch = FetchType.LAZY,mappedBy = "user")
	private List<Order> orders;
	public Long getUserId() {
		return UserId;
	}
	public void setUserId(Long userId) {
		UserId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	
	

}
