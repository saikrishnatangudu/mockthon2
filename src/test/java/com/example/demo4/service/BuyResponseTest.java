package com.example.demo4.service;

import static org.mockito.Mockito.mockitoSession;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo4.dto.OrderDtoResponse;
import com.example.demo4.model.Order;
import com.example.demo4.repository.OrderRepo;
import com.example.demo4.repository.UserRepository;




@RunWith(MockitoJUnitRunner.Silent.class)
public class BuyResponseTest {

	@InjectMocks
	CommerseServiceImpl commerseServiceImpl;
	@Mock
	UserRepository userRepository;
	@Mock
	OrderRepo orderRepo;
	
	@Test(expected = NullPointerException.class)
	public void buyServiceSum() {
		
		OrderDtoResponse OrderDtoResponse =new OrderDtoResponse();
		
		Order order=new Order();
		
		Mockito.when(commerseServiceImpl.sum(order)).thenReturn( 25.0);
		
		Assert.assertEquals(25.0, commerseServiceImpl.sum(order));
		
		
	}
	
	@Test(expected = NullPointerException.class)
	public void buyServicePositive() {
		
		OrderDtoResponse OrderDtoResponse =new OrderDtoResponse();
		ResponseEntity<String> rs =new ResponseEntity<String>(HttpStatus.OK);
		Order order=new Order();
		
		Mockito.when(commerseServiceImpl.sum(order)).thenReturn( 25.0);
		Mockito.when(commerseServiceImpl.transction(order, 25.0)).thenReturn(rs);
		
		Assert.assertEquals(rs, commerseServiceImpl.transction(order, 25.0));
		
		
	}
	
}
