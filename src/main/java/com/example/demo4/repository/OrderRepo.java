package com.example.demo4.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo4.model.Order;



public interface OrderRepo extends JpaRepository<Order, Integer> {
	
	

}
